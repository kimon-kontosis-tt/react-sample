const validators = {
  username: {
    regex: /^[A-Za-z][A-Za-z\d_]{2,31}$/,
    message: 'Username must be 3-32 letters, numbers or `_`, starting with a letter',
  },
  password: {
    regex: /^.{8,32}$/,
    message: 'Password must be 8-32 characters',
  },
  name: {
    regex: /.{3,50}$/,
    message: 'Name must be 3-50 characters',
  },
  email: {
    regex: /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
    message: 'A valid e-mail address must be provided',
  },
  id: {
    regex: /^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i,
    message: 'A valid id must be provided',
  },
  title: {
    maxLength: 256,
    message: 'The title length exceeds the limit of 256 characters',
  },
  body: {
    maxLength: 4096,
    message: 'The comment body exceeds the limit of 4KB',
  },
};

module.exports = {
  validators,
};
