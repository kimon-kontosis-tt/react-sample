const axios = require('axios');
const config = require('../server/config/constants');

// Helper class that makes requests to the local server and stores the authToken.

class Client {
  constructor() {
    this.authToken = null;
  }

  /**
   * Make a request to the local server for testing purposes
   *
   * @param {String} method      - the http method
   * @param {String} uri         - the absolute path to the api endpoint
   * @param {Object} [data]      - the body to post
   * @param {String} [authToken] - the authorization token
   */
  request(method, uri, data) {
    return axios({
      url: config.app.url + uri,
      method,
      data,
      headers: (this.authToken && { Authorization: `Bearer ${this.authToken}` }) || {},
    });
  }
}

function set(method, func) {
  Client.prototype[method] = func;
}

['get', 'post', 'put', 'delete'].forEach(method => set(method, function (...args) {
  return this.request(method, ...args);
}));

module.exports = {
  Client,
};
