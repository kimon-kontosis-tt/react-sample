import React from 'react';
import { Provider } from 'react-redux';
import ReactDom from 'react-dom';
import injectTapEventPlugin from 'react-tap-event-plugin';
import AppRouter from './Router';
import configureStore from './middleware/configureStore';

injectTapEventPlugin();

const store = configureStore();

ReactDom.render(
  <Provider store={store}>
    <div>
      <AppRouter />
    </div>
  </Provider>, document.getElementById('start'));

if (process.env.NODE_ENV !== 'production') {
  // eslint-disable-next-line global-require
  const showDevToolsModule = require('./middleware/showDevTools');
  const showDevTools = showDevToolsModule.default || showDevToolsModule;
  showDevTools(store);
}
