import React from 'react';
import PropTypes from 'prop-types';
import { hashHistory } from 'react-router';
import * as mui from 'material-ui';
import RepairTable from '../components/RepairTable';

const style = {
  margin: 12,
};

export default class ListRepairs extends React.Component {
  constructor(props) {
    super(props);
    this.edit = this.edit.bind(this);
  }

  componentDidMount() {
    this.props.onLoad();
  }

  edit() {
    hashHistory.push(`/manager/repairs/view/${this.props.repairs.selected}`);
  }

  render() {
    return (
      <mui.Paper style={{ padding: '10px 100px 200px 100px' }}>
        <h2>Repairs</h2>
        <hr />
        <mui.RaisedButton
          label="Delete Selected"
          primary
          style={style}
          onClick={this.del}
        />
        <mui.RaisedButton
          label="Edit Selected"
          secondary
          style={style}
          onClick={this.edit}
        />

        <RepairTable
          rows={this.props.repairs.repairItems}
          users={this.props.users}
          onSelect={this.props.onSelect}
          selected={this.props.repairs.selected}
        />

      </mui.Paper>
    );
  }
}

ListRepairs.propTypes = {
  onLoad: PropTypes.func.isRequired,
  users: PropTypes.object.isRequired,
  repairs: PropTypes.object.isRequired,
};
