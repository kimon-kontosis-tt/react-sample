import React from 'react';
import PropTypes from 'prop-types';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import NavMenu from './containers/NavMenu';
import MainSnackBar from './containers/MainSnackBar';
import Alert from './containers/Alert';
import Loader from './containers/Loader';

export default class App extends React.Component {
  render() {
    return (
      <MuiThemeProvider>
        <div>
          <NavMenu />
          <Loader />
          <Alert />
          {this.props.children}
          <MainSnackBar />
        </div>
      </MuiThemeProvider>
    );
  }
}

App.propTypes = {
  children: PropTypes.node.isRequired,
};

