const initialState = {
  lastMessage: null,
};

export default function info(state = initialState, action) {
  switch (action.type) {
    case 'INFO':
      return { lastMessage: action.payload.message };

    case 'REGISTER_SUCCESS':
      return { lastMessage: 'User successfully registered' };

    case 'REPAIR_CREATE_SUCCESS':
      return { lastMessage: 'Repair successfully created' };

    default:
      return state;
  }
}
