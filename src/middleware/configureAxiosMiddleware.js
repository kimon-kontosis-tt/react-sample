import axios from 'axios';
import axiosMiddleware from 'redux-axios-middleware';

const restClient = axios.create({
  baseURL: '/api',
  responseType: 'json',
});

export default axiosMiddleware(restClient);
