import _ from 'lodash';
import { connect } from 'react-redux';
import Register from '../../screens/Register';
import * as otherActions from '../../actions/other';
import * as apiActions from '../../actions/api';

const mapStateToProps = state => ({
  profile: state.profile,
});

const mapDispatchToProps = (disp, state) => ({
  onSubmit: model => disp((dispatch, getState) => {
    const state = getState();

    if (model.password !== model.password2) {
      return dispatch(otherActions.alert('Passwords do not match'));
    }

    const payload = _.omit(model, 'password2');

    return dispatch(apiActions.register(payload));
  }),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Register);
