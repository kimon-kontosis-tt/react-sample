import { connect } from 'react-redux';
import ListRepairs from '../../../screens/ListRepairs';
import * as otherActions from '../../../actions/other';
import * as apiActions from '../../../actions/api';

const mapStateToProps = state => ({
  users: state.users,
  repairs: state.repairs,
});


const mapDispatchToProps = (disp, state) => ({
  onLoad: () => disp((dispatch, getState) => {
    const state = getState();

    // TODO: consider including filters from state.users.filters
    dispatch(apiActions.repairList({
      order: 'date',
    }));

    // load all the users as well
    dispatch(apiActions.userList({
      order: 'username',
    }));
  }),
  onSelect: row => disp(otherActions.repairSelect(row._id)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ListRepairs);
