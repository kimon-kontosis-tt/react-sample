import { connect } from 'react-redux';
import BaseAlert from '../components/BaseAlert';
import * as otherActions from '../actions/other';

const mapStateToProps = state => ({
  message: state.requestStatus.error,
});

const mapDispatchToProps = dispatch => ({
  onClose: () => {
    dispatch(otherActions.confirmAlert());
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(BaseAlert);

