import { connect } from 'react-redux';
import BaseLoader from '../components/BaseLoader';
import * as otherActions from '../actions/other';

const mapStateToProps = state => ({
  enabled: state.requestStatus.isLoading,
});

export default connect(
  mapStateToProps,
)(BaseLoader);

