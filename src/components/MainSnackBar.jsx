import * as mui from 'material-ui';
import React from 'react';
import PropTypes from 'prop-types';

export default class MainSnackBar extends React.Component {
  constructor(props) {
    super(props);
    this.handleRequestClose = this.handleRequestClose.bind(this);
  }

  handleRequestClose() {
    this.props.onCloseInfo();
  }

  render() {
    return (
      <mui.Snackbar
        open={Boolean(this.props.info.lastMessage)}
        message={this.props.info.lastMessage || ''}
        autoHideDuration={3000}
        onRequestClose={this.handleRequestClose}
      />
    );
  }
}

MainSnackBar.propTypes = {
  info: PropTypes.object,
  onCloseInfo: PropTypes.func.isRequired,
};

MainSnackBar.defaultProps = {
  info: { lastMessage: '' },
};
