import _ from 'lodash';
import React from 'react';
import PropTypes from 'prop-types';
import { hashHistory } from 'react-router';
import * as mui from 'material-ui';
import constants from '../../common/constants';
import BaseForm from './BaseForm';
import NameWithAvatar from './NameWithAvatar';

const schema = {
  type: 'object',
  title: 'Send Comment',
  properties: {
    body: {
      title: 'Body',
      type: 'string',
      minLength: 1,
    },
  },
  required: ['body'],
};

const form = [
  'body'
];

export default class CommentForm extends React.Component {
  render() {

    return (
      <div>
        <br />
        <br />
        <mui.List style={{width: '100%', margin: 'auto'}} >
        {
          [...this.props.item.comments].map((c) => {
            <mui.ListItem>
              <NameWithAvatar value={
                  <span>{c.name}:<span style={{fontSize: '80%'}}>{c.body}</span></span>
                }
                photoUrl={c.photoUrl}
              />
            </mui.ListItem>
          })
        }
        </mui.List>
        <hr />
        <BaseForm
          schema={schema}
          form={form}
          model={{}}
          submitLabel="Comment"
          validate={true}
          onSubmit={this.props.onSend}
        />
      </div>
    );
  }
}

CommentForm.propTypes = {
  users: PropTypes.object.isRequired,
  item: PropTypes.object.isRequired,
  onSend: PropTypes.func.isRequired,
};
