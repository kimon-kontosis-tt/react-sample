import React from 'react';
import PropTypes from 'prop-types';

export default class EnsureRole extends React.Component {
  componentDidMount() {
    if (!this.props.isLoggedIn) {
      this.context.router.push('/');
      // TODO: browserHistory.replace("/")
    }
  }

  render() {
    if (this.props.isLoggedIn) {
      return this.props.children;
    }
    return null;
  }
}

EnsureRole.propTypes = {
  isLoggedIn: PropTypes.bool.isRequired,
  children: PropTypes.node.isRequired,
};

EnsureRole.contextTypes = {
  router: PropTypes.object.isRequired,
};
