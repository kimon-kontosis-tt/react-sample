import React from 'react';
import PropTypes from 'prop-types';
import * as mui from 'material-ui';
import ComposedComponent from 'react-schema-form/lib/ComposedComponent';

class TimePickerFormElement extends React.Component {
  constructor(props) {
    super(props);
    this.onTimePicked = this.onTimePicked.bind(this);
  }


  onTimePicked(empty, date) {
    this.props.onChangeValidate({
      target: {
        value: date,
      },
    });
  }

  render() {
    let value = null;
    if (this.props && this.props.value) {
      value = this.props.value;
    }

    return (
      <div style={{ width: '100%', display: 'block' }} className={this.props.form.htmlClass}>
        <mui.TimePicker
          mode={'landscape'}
          autoOk
          floatingLabelText={this.props.form.title}
          hintText={this.props.form.title}
          onChange={this.onTimePicked}
          onShow={null}
          onDismiss={null}
          value={value}
          disabled={this.props.form.readonly}
          style={this.props.form.style || { width: '100%' }}
        />

      </div>
    );
  }
}


export default ComposedComponent(TimePickerFormElement);
