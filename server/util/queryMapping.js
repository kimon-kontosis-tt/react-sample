const _ = require('lodash');

/**
 * Get mongoose sorting options based on API payload
 *
 * @param {Object} payload    - the provided API payload
 * @param {String} defOrderBy - the default field to sort by if none is provided
 * @returns {Object} opts     - the mongoose sorting options object
 */
function getSortOptions(payload, defOrderBy) {
  const opts = {
    sort: {
    },
  };

  const sortBy = payload.order || defOrderBy;
  opts.sort[sortBy] = 1;

  if (payload.offset !== undefined) {
    opts.skip = payload.offset;
  }

  if (payload.page !== undefined) {
    opts.limit = payload.page;
  }

  return opts;
}

/**
 * Get mongoose query object based on API payload and a custom map
 *
 * @param {Object} payload        - the provided API payload
 * @param {Object} map            - the custom map of each input field to a corresponding query key
 * @param {Array}  map.field      -   A tuple of 0-2 items describing how `field` should be mapped
 * @param {String} [map.field[0]] -     The object path in the destination query, default: `field`
 * @param {Function} [map.field[1]] - A mapper function for the value, default: identity function
 * @returns {Object} opts         - the mongoose query object
 */
function mapPayloadToQuery(payload, map) {
  const query = {};
  _.forEach(payload, (value, key) => {
    if (map[key]) {
      const transformation = map[key];
      const queryPath = transformation[0] || key;
      const queryValue = transformation[1] ? transformation[1](value) : value;
      _.set(query, queryPath, queryValue);
    }
  });

  return query;
}


module.exports = {
  getSortOptions,
  mapPayloadToQuery,
};
