const _ = require('lodash');
const assert = require('assert');
const bcrypt = require('bcrypt');
const config = require('../config/constants');
const constants = require('../util/constants');
const mongoose = require('mongoose');
const Promise = require('bluebird');

const Schema = mongoose.Schema;

const UserSchema = new Schema({
  username: {
    type: String,
    validate: {
      validator: v => constants.validators.username.regex.test(v),
      message: constants.validators.username.message,
    },
    required: true,
    index: true,
    unique: true,
  },
  hashedPassword: {
    type: String,
    select: false,
  },
  role: {
    type: String,
    enum: constants.roles,
    default: 'user',
  },
  email: {
    type: String,
    validate: {
      validator: v => constants.validators.email.regex.test(v),
      message: constants.validators.email.message,
    },
    required: true,
    index: true,
    unique: true,
  },
  name: {
    type: String,
    validate: {
      validator: v => constants.validators.name.regex.test(v),
      message: constants.validators.name.message,
    },
    required: true,
  },
  photoUrl: {
    type: String,
  },
  createdOn: {
    type: Date,
    required: true,
    default: Date.now,
    index: true,
  },
}, { versionKey: false });

UserSchema
  .virtual('password')
  .set(function setPassword(password) {
    this._password = password;
  })
  .get(function getPassword() {
    return this._password;
  });

UserSchema
  .pre('save', function hashPasswordBeforeSave(cb) {
    // no password is defined to be saved
    if (!this.password) return cb();

    return Promise.resolve(this.encryptPassword()).asCallback(cb);
  });

UserSchema.methods.comparePassword = function comparePassword(password) {
  return bcrypt.compare(password, this.hashedPassword).then((success) => {
    // Verbose logging of failed login will be made at the error middleware
    if (!success) throw Object.assign(new Error('Invalid login'), { statusCode: 401 });
    return _.omit(this.toObject(), 'hashedPassword');
  });
};

UserSchema.methods.encryptPassword = function encryptPassword() {
  assert(config.bcrypt.saltRounds, 'Bug: bcrypt saltrounds is not defined');

  return bcrypt.hash(this.password, config.bcrypt.saltRounds).then((hashedPassword) => {
    this.hashedPassword = hashedPassword;
  });
};

UserSchema.methods.create = async function create() {
  const user = await this.save();
  return _.omit(user.toObject(), 'hashedPassword');
};

UserSchema.statics.testLogin = function testLogin({ username, password }) {
  return this.findOne({ username })
    .select('+hashedPassword')
    .then((user) => {
      if (!user) throw Object.assign(new Error('Invalid login'), { statusCode: 401 });

      return user.comparePassword(password);
    });
};

module.exports = mongoose.model('User', UserSchema);
