const _ = require('lodash');
const assert = require('assert');

const env = process.env.NODE_ENV || 'development';

function getAppConfig() {
  const result = {
    protocol: process.env.NODE_PROTOCOL || 'http',
    host: process.env.NODE_HOST,
    port: process.env.NODE_PORT,
  };

  // Set defaults for the development environment
  // All variables need to be defined for the production environment,
  // except for protocol which defaults to http
  if (env === 'development') {
    _.defaults(result, {
      host: '127.0.0.1',
      port: '8080',
    });
  }

  return {
    url: `${result.protocol}://${result.host}:${result.port}`,
    protocol: result.protocol,
    host: result.host,
    port: result.port,
  };
}

function getDbConfig() {
  const result = {
    uri: process.env.MONGO_URL,
  };

  // Set defaults for the development environment
  if (env === 'development') {
    _.defaults(result, {
      uri: 'mongodb://localhost/react_academy_dev',
    });
  }

  return result;
}

function getJwtConfig() {
  const result = {
    secretOrKey: process.env.JWT_KEY,
  };

  // Set default JWT secret for the development environment
  if (env === 'development') {
    _.defaults(result, {
      secretOrKey: 'hackme',
    });
  }

  return result;
}

const app = getAppConfig();
const db = getDbConfig();
const jwt = getJwtConfig();

const bcrypt = {
  saltRounds: 11,
};

assert(app.host, 'Missing environment variable: application host is not defined');
assert(app.port, 'Missing environment variable: application port is not defined');
assert(jwt.secretOrKey, 'Missing environment variable: jwt key is not defined');
assert(db.uri, 'Missing environment variable: mongo db uri is not defined');
assert(bcrypt.saltRounds);

module.exports = {
  env,
  app,
  db,
  jwt,
  bcrypt,
};
