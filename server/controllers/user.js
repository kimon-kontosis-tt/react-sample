const mongoose = require('mongoose');

/**
 * Get Users Controller
 *
 * @param {Object} req - express request object
 * @returns {Promise(responseData)}
 */
module.exports.get = async function getCtrl(req) {
  return mongoose.models.User.find({ _id: req.query.id });
};
