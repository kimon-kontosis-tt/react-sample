const path = require('path');
const webpack = require('webpack');
const WebpackNotifierPlugin = require('webpack-notifier');

module.exports = function getWebpackConfig(environment) {
  const isDev = environment !== 'production';

  const devEntry = isDev ? ['webpack-hot-middleware/client'] : [];
  const entry = [
    'babel-polyfill',
    ...devEntry,
    './src/index.jsx',
  ];

  const output = {
    path: path.resolve(__dirname, './public'),
    filename: './js/bundle.js',
    publicPath: '/',
  };

  const resolve = {
    extensions: ['.js', '.jsx'],
  };

  const devJsxLoader = isDev ? ['react-hot-loader'] : [];
  const module = {
    loaders: [
      {
        test: /\.jsx?$/,
        loaders: [...devJsxLoader, 'babel-loader'],
        exclude: /node_modules/,
      },
    ],
  };

  const devPlugins = [
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoEmitOnErrorsPlugin(),
    new WebpackNotifierPlugin({ alwaysNotify: true }),
  ];
  const plugins = [
    ...devPlugins,
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify(environment) || JSON.stringify('dev'),
    }),
  ];

  return {
    entry,
    output,
    resolve,
    module,
    plugins,
    devtool: 'eval',
  };
};
